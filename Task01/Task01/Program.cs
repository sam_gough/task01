﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task01
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = "";
            var age = 0;
            Console.WriteLine($"Please enter your name and press enter.");
            name = Console.ReadLine();
            Console.WriteLine($"Please enter your age and press enter.");
            age = int.Parse(Console.ReadLine());

            Console.WriteLine($"Thanks {name}, you are {age} years old.");
            Console.WriteLine("Thanks {0}, you are {1} years old", name, age);
        }
    }
}
